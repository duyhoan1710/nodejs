let passport = require('passport');
module.exports = {
    get : (req , res ) => {
        res.render('./login.view/login');
    },
    post : (req , res) =>{
        passport.authenticate('local' , {failureRedirect : '/login' , successRedirect : '/product'});
    }
};