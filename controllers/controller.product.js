let productController = require('../models/model.product');


module.exports = {
    get : (req , res , next )=>{
        productController.find({} , (err , product) =>{
            if(err) throw err ;
            res.render('./product.view/viewProduct' , {products : product});
        });
    },
    getOne : (req , res , next )=>{
        let _id = req.params._id;
        productController.findOne({_id} , (err , product) =>{
            if(err) throw err;
            console.log(_id);
            res.render('./product.view/updateProduct' , {products : product});
        });
    },
    post : (req , res , next ) =>{
        productController.create({
            product_link : req.body.product_link ,
            product_name : req.body.product_name ,
            product_cost : req.body.product_cost,
            product_description : req.body.product_description} , (err , product) =>{
            if(err) throw err;
            console.log(product);
            res.redirect('/product');
        });
    },

    update : (req ,res , next) =>{
        let _id = req.body._id;
        productController.updateOne({_id} , req.body ,(err , product) =>{
            if(err) throw err ;
            console.log(product);
            res.redirect('/product');
        });
    },
    delete : (req ,res , next) =>{
        let _id = req.params._id;
        productController.deleteOne({_id} , (err , result)=>{
            if(err) throw err ;
            console.log(result);
            res.redirect('/product');
        });
    }
};