let persionModel = require('../models/model.persion');

module.exports = {
    get : (req , res , next ) =>{
        persionModel.find({},(err , listPersion) =>{
            if(err)  throw err ;
            res.render('./persion.view/viewPersion' , {customers : listPersion});
        });
    },
    getOne : (req, res , next) =>{
        let _id = req.query._id;
        persionModel.findOne({_id} , (err , persion) =>{
            if(err) throw err ;
            res.render('./persion.view/updatePersion' , {customers : persion});
        });
    },
    post : (req , res , next ) =>{
        persionModel.create(({name : req.body.name , adress : req.body.adress ,phone: req.body.phone}), (err , perion) =>{
            if(err) res.redirect('./create');
            else {
                res.redirect('/persion');
            }
        });

    },
    update : (req , res , next)=>{
        let _id = req.body._id;
        console.log(_id);
        persionModel.updateOne({_id} , {name : req.body.name , adress : req.body.adress ,phone: req.body.phone} , { useFindAndModify: false }, (err , result) =>{
            if(err) throw err;
            console.log(_id);
            res.redirect('/persion');
        });
    },
    delete : (req , res , next )=>{
        let _id = req.query._id;
        persionModel.deleteOne({_id} , (err , result) =>{
            if(err) throw err;
            console.log(result);
            res.redirect('/persion');
        });
    }
};