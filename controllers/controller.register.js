let bcrypt = require('bcrypt');
let Persion = require('../models/model.persion');
let saltRounds = 10;

module.exports = {
    get : (req , res )=>{
        res.render('./login.view/register');
    },
    post :(req, res) =>{
        let user = req.body.username;
        let email = req.body.email;
        let pass = req.body.password;
        bcrypt.hash(pass, saltRounds , (err , hash) =>{
            if(err) {
                res.render('./login.view/register' , {info : req.body});
            }
            Persion.create({username : user , password : hash , email : email});
            res.redirect('/product');
        });
    }
};