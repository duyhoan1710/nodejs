let mongoose = require('mongoose');

let Product =new mongoose.Schema({
   product_link :{type : String , require : true},
   product_name : {type : String , required : true },
   product_cost : {type : Number , require: true  },
   product_description : {type : String , require : true}
});

module.exports = mongoose.model('product' , Product , 'product');