let mongoose = require('mongoose');
let product = require('../models/model.product');
let modelPersion = new  mongoose.Schema({
    username : { type : String, required : true},
    password : {type : String , required: true},
    email : {type : String , required: true},
    adress : {type : String , required: false},
    phone : {type : String , required: false},
    products : [{type : mongoose.Schema.Types.ObjectId, ref: product}]
});

module.exports = mongoose.model('persion' , modelPersion , 'customers');
