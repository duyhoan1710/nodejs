let express = require('express');
let mongoose  = require('mongoose');
let passport = require('passport');
let session = require('express-session');
let persionRouter = require('./routers/router.persion');
let productRouter = require('./routers/router.product');
let loginRouter = require('./routers/router.login');
require('./passport/PassPortSetUp');
// let bodyParse = require('body-parse');
// let persionController = require('./controllers/controller.persion')
let app = express();
app.use(session ({
    secret : 'mysecret',
    saveUninitialized : true,
    resave : true
}));
app.use(passport.initialize());
app.use(passport.session());
let url = "mongodb://localhost:27017/mydb";
mongoose.connect(url,{useNewUrlParser: true , useUnifiedTopology: true} , (err , db ) =>{
    if(err) throw err ;
    console.log('connect database');
});
app.set('view engine' , 'pug');
app.set('views' , './views');
app.use(express.static('public'));
app.use(express.urlencoded({extended : false }));
app.use('/persion' , persionRouter);
app.use('/product' , productRouter);
app.use('/' , loginRouter);

app.listen(5000, ()=>{
    console.log('server is running is post 5000');
});