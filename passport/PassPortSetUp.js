let passport = require('passport');
let LocalStrategy  = require('passport-local').Strategy;
let persionModel = require('../models/model.persion');
let bcrypt = require('bcrypt');

passport.use( 'local', new LocalStrategy({},
    (username , password , done)=>{
        persionModel.findOne({username : username} , (err , user)=>{
            if(err) {
                throw err;
            }
            if(!user){
                return done(null , false , {message : 'unknow User'});
            }
            bcrypt.compare(password , user.password , (err , isMatch)=>{
                if(err) throw err ;
                if(isMatch){
                    return done(null, user);
                }else{
                    return done(null, false , {message : 'Invalid password'});
                }
            });
        });
    }
));

passport.serializeUser((user  , done)=>{
    done (null , user.username);
});

passport.deserializeUser((user , done)=>{
    persionModel.findOne({username : user.username} , (err , user)=>{
        if(err) throw err ;
        if(!user) {
            return done(null , false);
        }
        return done(null, user);
    });
});


