let express = require('express');
let productController = require('../controllers/controller.product');
let routerProduct = express.Router();
let authentication = require('../passport/authenticate');

routerProduct.get('/' , productController.get);

routerProduct.get('/create' ,authentication ,(req,res) =>{
    res.render('./product.view/createProduct');
});
routerProduct.post('/create' ,authentication, productController.post);

routerProduct.get('/update' , authentication,productController.getOne);

routerProduct.post('/update' , authentication,productController.update);

routerProduct.get('/delete' , authentication, productController.delete);


module.exports = routerProduct;