let express = require('express');
let persionController = require('../controllers/controller.persion');
let Router = express.Router();
let authentication = require('../passport/authenticate');
Router.get('/' ,authentication, persionController.get);

Router.get('/create' , authentication,  (req , res) =>{
    res.render('./persion.view/createPersion');
});
Router.post('/create' , authentication,persionController.post);

Router.get('/delete' , authentication,persionController.delete);

Router.get('/update' ,authentication , persionController.getOne);

Router.post('/update' , authentication,persionController.update);

module.exports = Router;