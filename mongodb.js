let MongoClient = require('mongodb').MongoClient;
let url = "mongodb://localhost:27017/mydb";

MongoClient.connect(url,{ useUnifiedTopology: true }, function(err, db) {
    if (err) throw err;
    console.log('database is create !');
    let dbo = db.db("mydb");
    dbo.createCollection("customers" , (err , res) => {
        if (err) throw err;
        console.log('colection is create !');
    });
    dbo.createCollection("product" , (err , res) => {
        if (err) throw err;
        console.log('colection is create !');
    });
});
